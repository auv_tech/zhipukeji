// 翻译
var translation_mainpage = document.getElementById("translation_mainpage");
range[12].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    translation_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var translation_nav = document.getElementsByClassName("translation_nav");
var translation_directlink = document.getElementById("translation_directlink");

var translation_fengexian = document.getElementById("translation_fengexian");
var translation_searchbox = document.getElementById("range_search_box_translation");
translation_fengexian.style.marginLeft = '280px';
translation_searchbox.style.paddingLeft = '320px';

translation_nav[0].onclick = function (e) {
    translation_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    translation_nav[0].style.color = 'black';
    for (var j = 0; j < translation_nav.length; j++) {
        if (j != 0) {
            translation_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            translation_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
translation_nav[1].onclick = function (e) {
    translation_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    translation_nav[1].style.color = 'black';
    for (var j = 0; j < translation_nav.length; j++) {
        if (j != 1) {
            translation_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            translation_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
translation_nav[2].onclick = function (e) {
    translation_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    translation_nav[2].style.color = 'black';
    for (var j = 0; j < translation_nav.length; j++) {
        if (j != 2) {
            translation_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            translation_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}

var translation_link_wrap = document.getElementById("translation_link_wrap");
var translation_more = document.getElementById("translation_more");
var translation_more_font = document.getElementById("translation_more_font");
var translation_directlink_products = document.getElementsByClassName("range_total_directlink_products_translation");


translation_more.onclick = function (e) {
    if (translation_more_font.translationContent == '更多') {
        translation_directlink.style.height = '240px';
        translation_link_wrap.style.height = '240px';
        translation_directlink.style.transition = '0.5s';
        translation_more_font.style.marginTop = '210px';
        translation_more_font.style.transition = '0.5s';
        translation_more.style.marginTop = '205px';
        translation_more.style.transition = '0.5s';
        translation_more_font.translationContent = '收起';
        translation_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= translation_directlink_products.length; k++) {
            translation_directlink_products[k].style.display = 'flex';
            translation_directlink_products[k].style.transition = '0.5s';
            translation_directlink_products[k].style.opacity = '1';
        }
    }
    else if (translation_more_font.translationContent == '收起') {
        translation_directlink.style.height = '80px';
        translation_link_wrap.style.height = '80px';
        translation_directlink.style.transition = '0.5s';
        translation_more_font.style.marginTop = '50px';
        translation_more_font.style.transition = '0.5s';
        translation_more.style.marginTop = '45px';
        translation_more.style.transition = '0.5s';
        translation_more_font.translationContent = '更多';
        translation_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= translation_directlink_products.length; k++) {
            translation_directlink_products[k].style.display = 'flex';
            translation_directlink_products[k].style.transition = '0.5s';
            translation_directlink_products[k].style.opacity = '0';
        }
    }
}

var translation_choices = document.getElementsByClassName("translation_products_choice");
translation_choices[0].onclick = function (e) {
    translation_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    translation_choices[0].style.color = 'black';
    translation_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    translation_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    translation_choices[1].style.color = 'rgba(255,255,255,0.8)';
    translation_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    translation_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    translation_choices[2].style.color = 'rgba(255,255,255,0.8)';
    translation_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
translation_choices[1].onclick = function (e) {
    translation_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    translation_choices[1].style.color = 'black';
    translation_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    translation_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    translation_choices[0].style.color = 'rgba(255,255,255,0.8)';
    translation_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    translation_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    translation_choices[2].style.color = 'rgba(255,255,255,0.8)';
    translation_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var translation_choices2 = document.getElementById('translation_choice3');
    if (translation_choices2) {
        translation_choices2.addEventListener('click', function () {
            translation_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            translation_choices2.style.color = 'black';
            translation_choices2.style.border = '1px solid rgba(255,255,255,0)';
            translation_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            translation_choices[0].style.color = 'rgba(255,255,255,0.8)';
            translation_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            translation_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            translation_choices[1].style.color = 'rgba(255,255,255,0.8)';
            translation_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
translation_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(translation_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        translation_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        translation_choices[3].style.color = 'black';
        translation_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        translation_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        translation_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        translation_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}