// 制造
var manufacture_mainpage = document.getElementById("manufacture_mainpage");
range[5].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    manufacture_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var manufacture_nav = document.getElementsByClassName("manufacture_nav");
var manufacture_directlink = document.getElementById("manufacture_directlink");
manufacture_nav[0].onclick = function (e) {
    manufacture_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    manufacture_nav[0].style.color = 'black';
    for (var j = 0; j < manufacture_nav.length; j++) {
        if (j != 0) {
            manufacture_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            manufacture_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
manufacture_nav[1].onclick = function (e) {
    manufacture_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    manufacture_nav[1].style.color = 'black';
    for (var j = 0; j < manufacture_nav.length; j++) {
        if (j != 1) {
            manufacture_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            manufacture_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
manufacture_nav[2].onclick = function (e) {
    manufacture_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    manufacture_nav[2].style.color = 'black';
    for (var j = 0; j < manufacture_nav.length; j++) {
        if (j != 2) {
            manufacture_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            manufacture_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
manufacture_nav[3].onclick = function (e) {
    manufacture_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    manufacture_nav[3].style.color = 'black';
    for (var j = 0; j < manufacture_nav.length; j++) {
        if (j != 3) {
            manufacture_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            manufacture_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
manufacture_nav[4].onclick = function (e) {
    manufacture_nav[4].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    manufacture_nav[4].style.color = 'black';
    for (var j = 0; j < manufacture_nav.length; j++) {
        if (j != 4) {
            manufacture_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            manufacture_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
manufacture_nav[5].onclick = function (e) {
    manufacture_nav[5].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    manufacture_nav[5].style.color = 'black';
    for (var j = 0; j < manufacture_nav.length; j++) {
        if (j != 5) {
            manufacture_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            manufacture_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
var manufacture_more = document.getElementById("manufacture_more");
var manufacture_more_font = document.getElementById("manufacture_more_font");
manufacture_more.onclick = function (e) {
    if (manufacture_more_font.textContent == '更多') {
        manufacture_directlink.style.height = '240px';
        manufacture_directlink.style.transition = '0.5s';
        manufacture_more_font.style.marginTop = '210px';
        manufacture_more_font.style.transition = '0.5s';
        manufacture_more.style.marginTop = '205px';
        manufacture_more.style.transition = '0.5s';
        manufacture_more_font.textContent = '收起';
        manufacture_more.style.transform = 'rotate(0deg)';
    }
    else if (manufacture_more_font.textContent == '收起') {
        manufacture_directlink.style.height = '80px';
        manufacture_directlink.style.transition = '0.5s';
        manufacture_more_font.style.marginTop = '50px';
        manufacture_more_font.style.transition = '0.5s';
        manufacture_more.style.marginTop = '45px';
        manufacture_more.style.transition = '0.5s';
        manufacture_more_font.textContent = '更多';
        manufacture_more.style.transform = 'rotate(180deg)';
    }
}
var manufacture_choices = document.getElementsByClassName("manufacture_products_choice");
manufacture_choices[0].onclick = function (e) {
    manufacture_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    manufacture_choices[0].style.color = 'black';
    manufacture_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    manufacture_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    manufacture_choices[1].style.color = 'rgba(255,255,255,0.8)';
    manufacture_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    manufacture_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    manufacture_choices[2].style.color = 'rgba(255,255,255,0.8)';
    manufacture_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
manufacture_choices[1].onclick = function (e) {
    manufacture_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    manufacture_choices[1].style.color = 'black';
    manufacture_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    manufacture_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    manufacture_choices[0].style.color = 'rgba(255,255,255,0.8)';
    manufacture_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    manufacture_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    manufacture_choices[2].style.color = 'rgba(255,255,255,0.8)';
    manufacture_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var manufacture_choices2 = document.getElementById('manufacture_choice3');
    if (manufacture_choices2) {
        manufacture_choices2.addEventListener('click', function () {
            manufacture_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            manufacture_choices2.style.color = 'black';
            manufacture_choices2.style.border = '1px solid rgba(255,255,255,0)';
            manufacture_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            manufacture_choices[0].style.color = 'rgba(255,255,255,0.8)';
            manufacture_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            manufacture_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            manufacture_choices[1].style.color = 'rgba(255,255,255,0.8)';
            manufacture_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
manufacture_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(manufacture_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        manufacture_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        manufacture_choices[3].style.color = 'black';
        manufacture_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        manufacture_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        manufacture_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        manufacture_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}
