// 程序开发
var code_mainpage = document.getElementById("code_mainpage");
range[13].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    code_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var code_nav = document.getElementsByClassName("code_nav");
var code_directlink = document.getElementById("code_directlink");

var code_fengexian = document.getElementById("code_fengexian");
var code_searchbox = document.getElementById("range_search_box_code");
code_fengexian.style.marginLeft = '280px';
code_searchbox.style.paddingLeft = '320px';

code_nav[0].onclick = function (e) {
    code_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    code_nav[0].style.color = 'black';
    for (var j = 0; j < code_nav.length; j++) {
        if (j != 0) {
            code_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            code_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
code_nav[1].onclick = function (e) {
    code_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    code_nav[1].style.color = 'black';
    for (var j = 0; j < code_nav.length; j++) {
        if (j != 1) {
            code_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            code_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
code_nav[2].onclick = function (e) {
    code_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    code_nav[2].style.color = 'black';
    for (var j = 0; j < code_nav.length; j++) {
        if (j != 2) {
            code_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            code_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}

var code_link_wrap = document.getElementById("code_link_wrap");
var code_more = document.getElementById("code_more");
var code_more_font = document.getElementById("code_more_font");
var code_directlink_products = document.getElementsByClassName("range_total_directlink_products_code");


code_more.onclick = function (e) {
    if (code_more_font.codeContent == '更多') {
        code_directlink.style.height = '240px';
        code_link_wrap.style.height = '240px';
        code_directlink.style.transition = '0.5s';
        code_more_font.style.marginTop = '210px';
        code_more_font.style.transition = '0.5s';
        code_more.style.marginTop = '205px';
        code_more.style.transition = '0.5s';
        code_more_font.codeContent = '收起';
        code_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= code_directlink_products.length; k++) {
            code_directlink_products[k].style.display = 'flex';
            code_directlink_products[k].style.transition = '0.5s';
            code_directlink_products[k].style.opacity = '1';
        }
    }
    else if (code_more_font.codeContent == '收起') {
        code_directlink.style.height = '80px';
        code_link_wrap.style.height = '80px';
        code_directlink.style.transition = '0.5s';
        code_more_font.style.marginTop = '50px';
        code_more_font.style.transition = '0.5s';
        code_more.style.marginTop = '45px';
        code_more.style.transition = '0.5s';
        code_more_font.codeContent = '更多';
        code_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= code_directlink_products.length; k++) {
            code_directlink_products[k].style.display = 'flex';
            code_directlink_products[k].style.transition = '0.5s';
            code_directlink_products[k].style.opacity = '0';
        }
    }
}

var code_choices = document.getElementsByClassName("code_products_choice");
code_choices[0].onclick = function (e) {
    code_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    code_choices[0].style.color = 'black';
    code_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    code_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    code_choices[1].style.color = 'rgba(255,255,255,0.8)';
    code_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    code_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    code_choices[2].style.color = 'rgba(255,255,255,0.8)';
    code_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
code_choices[1].onclick = function (e) {
    code_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    code_choices[1].style.color = 'black';
    code_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    code_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    code_choices[0].style.color = 'rgba(255,255,255,0.8)';
    code_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    code_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    code_choices[2].style.color = 'rgba(255,255,255,0.8)';
    code_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var code_choices2 = document.getElementById('code_choice3');
    if (code_choices2) {
        code_choices2.addEventListener('click', function () {
            code_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            code_choices2.style.color = 'black';
            code_choices2.style.border = '1px solid rgba(255,255,255,0)';
            code_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            code_choices[0].style.color = 'rgba(255,255,255,0.8)';
            code_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            code_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            code_choices[1].style.color = 'rgba(255,255,255,0.8)';
            code_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
code_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(code_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        code_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        code_choices[3].style.color = 'black';
        code_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        code_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        code_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        code_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}