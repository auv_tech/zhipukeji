// 交通
var traffic_mainpage = document.getElementById("traffic_mainpage");
range[7].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    traffic_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var traffic_nav = document.getElementsByClassName("traffic_nav");
var traffic_directlink = document.getElementById("traffic_directlink");
traffic_nav[0].onclick = function (e) {
    traffic_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[0].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 0) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
traffic_nav[1].onclick = function (e) {
    traffic_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[1].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 1) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
traffic_nav[2].onclick = function (e) {
    traffic_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[2].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 2) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
traffic_nav[3].onclick = function (e) {
    traffic_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[3].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 3) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
traffic_nav[4].onclick = function (e) {
    traffic_nav[4].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[4].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 4) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
traffic_nav[5].onclick = function (e) {
    traffic_nav[5].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[5].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 5) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
traffic_nav[6].onclick = function (e) {
    traffic_nav[6].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    traffic_nav[6].style.color = 'black';
    for (var j = 0; j < traffic_nav.length; j++) {
        if (j != 6) {
            traffic_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            traffic_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
var traffic_more = document.getElementById("traffic_more");
var traffic_more_font = document.getElementById("traffic_more_font");
traffic_more.onclick = function (e) {
    if (traffic_more_font.textContent == '更多') {
        traffic_directlink.style.height = '240px';
        traffic_directlink.style.transition = '0.5s';
        traffic_more_font.style.marginTop = '210px';
        traffic_more_font.style.transition = '0.5s';
        traffic_more.style.marginTop = '205px';
        traffic_more.style.transition = '0.5s';
        traffic_more_font.textContent = '收起';
        traffic_more.style.transform = 'rotate(0deg)';
    }
    else if (traffic_more_font.textContent == '收起') {
        traffic_directlink.style.height = '80px';
        traffic_directlink.style.transition = '0.5s';
        traffic_more_font.style.marginTop = '50px';
        traffic_more_font.style.transition = '0.5s';
        traffic_more.style.marginTop = '45px';
        traffic_more.style.transition = '0.5s';
        traffic_more_font.textContent = '更多';
        traffic_more.style.transform = 'rotate(180deg)';
    }
}
var traffic_choices = document.getElementsByClassName("traffic_products_choice");
traffic_choices[0].onclick = function (e) {
    traffic_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    traffic_choices[0].style.color = 'black';
    traffic_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    traffic_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    traffic_choices[1].style.color = 'rgba(255,255,255,0.8)';
    traffic_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    traffic_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    traffic_choices[2].style.color = 'rgba(255,255,255,0.8)';
    traffic_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
traffic_choices[1].onclick = function (e) {
    traffic_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    traffic_choices[1].style.color = 'black';
    traffic_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    traffic_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    traffic_choices[0].style.color = 'rgba(255,255,255,0.8)';
    traffic_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    traffic_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    traffic_choices[2].style.color = 'rgba(255,255,255,0.8)';
    traffic_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var traffic_choices2 = document.getElementById('traffic_choice3');
    if (traffic_choices2) {
        traffic_choices2.addEventListener('click', function () {
            traffic_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            traffic_choices2.style.color = 'black';
            traffic_choices2.style.border = '1px solid rgba(255,255,255,0)';
            traffic_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            traffic_choices[0].style.color = 'rgba(255,255,255,0.8)';
            traffic_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            traffic_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            traffic_choices[1].style.color = 'rgba(255,255,255,0.8)';
            traffic_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
traffic_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(traffic_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        traffic_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        traffic_choices[3].style.color = 'black';
        traffic_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        traffic_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        traffic_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        traffic_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}
