// 政府
var govern_mainpage = document.getElementById("govern_mainpage");
range[4].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    govern_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var govern_nav = document.getElementsByClassName("govern_nav");
var govern_directlink = document.getElementById("govern_directlink");
govern_nav[0].onclick = function (e) {
    govern_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    govern_nav[0].style.color = 'black';
    for (var j = 0; j < govern_nav.length; j++) {
        if (j != 0) {
            govern_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            govern_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
govern_nav[1].onclick = function (e) {
    govern_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    govern_nav[1].style.color = 'black';
    for (var j = 0; j < govern_nav.length; j++) {
        if (j != 1) {
            govern_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            govern_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
govern_nav[2].onclick = function (e) {
    govern_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    govern_nav[2].style.color = 'black';
    for (var j = 0; j < govern_nav.length; j++) {
        if (j != 2) {
            govern_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            govern_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
govern_nav[3].onclick = function (e) {
    govern_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    govern_nav[3].style.color = 'black';
    for (var j = 0; j < govern_nav.length; j++) {
        if (j != 3) {
            govern_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            govern_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}

var govern_more = document.getElementById("govern_more");
var govern_more_font = document.getElementById("govern_more_font");
var govern_directlink_products = document.getElementsByClassName("range_total_directlink_products_govern");


govern_more.onclick = function (e) {
    if (govern_more_font.textContent == '更多') {
        govern_directlink.style.height = '160px';
        govern_directlink.style.transition = '0.5s';
        govern_more_font.style.marginTop = '130px';
        govern_more_font.style.transition = '0.5s';
        govern_more.style.marginTop = '125px';
        govern_more.style.transition = '0.5s';
        govern_more_font.textContent = '收起';
        govern_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= govern_directlink_products.length; k++) {
            govern_directlink_products[k].style.display = 'flex';
            govern_directlink_products[k].style.transition = '0.5s';
            govern_directlink_products[k].style.opacity = '1';
        }
    }
    else if (govern_more_font.textContent == '收起') {
        govern_directlink.style.height = '80px';
        govern_directlink.style.transition = '0.5s';
        govern_more_font.style.marginTop = '50px';
        govern_more_font.style.transition = '0.5s';
        govern_more.style.marginTop = '45px';
        govern_more.style.transition = '0.5s';
        govern_more_font.textContent = '更多';
        govern_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= govern_directlink_products.length; k++) {
            govern_directlink_products[k].style.display = 'flex';
            govern_directlink_products[k].style.transition = '0.5s';
            govern_directlink_products[k].style.opacity = '0';
        }
    }
}
var govern_choices = document.getElementsByClassName("govern_products_choice");
govern_choices[0].onclick = function (e) {
    govern_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    govern_choices[0].style.color = 'black';
    govern_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    govern_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    govern_choices[1].style.color = 'rgba(255,255,255,0.8)';
    govern_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    govern_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    govern_choices[2].style.color = 'rgba(255,255,255,0.8)';
    govern_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
govern_choices[1].onclick = function (e) {
    govern_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    govern_choices[1].style.color = 'black';
    govern_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    govern_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    govern_choices[0].style.color = 'rgba(255,255,255,0.8)';
    govern_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    govern_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    govern_choices[2].style.color = 'rgba(255,255,255,0.8)';
    govern_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var govern_choices2 = document.getElementById('govern_choice3');
    if (govern_choices2) {
        govern_choices2.addEventListener('click', function () {
            govern_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            govern_choices2.style.color = 'black';
            govern_choices2.style.border = '1px solid rgba(255,255,255,0)';
            govern_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            govern_choices[0].style.color = 'rgba(255,255,255,0.8)';
            govern_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            govern_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            govern_choices[1].style.color = 'rgba(255,255,255,0.8)';
            govern_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
govern_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(govern_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        govern_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        govern_choices[3].style.color = 'black';
        govern_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        govern_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        govern_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        govern_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}
