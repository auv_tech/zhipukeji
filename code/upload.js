// 获取元素
var upload_btn = document.getElementById('signin_submit_button');
var upload_phone = document.getElementById('upload_phone');
var upload_password = document.getElementById('upload_password');
var register_btn=document.getElementById('register_submit_button');
var register_phone=document.getElementById('register_phone');
var register_password=document.getElementById('register_password');
var register_password_confrim = document.getElementById('register_pw_confirm');

// 注册
register_btn.onclick = function (e) {
    var register_phone_value = register_phone.value;
    var register_password_value = register_password.value;
    var register_password_confrim_value=register_password_confrim.value;

    // 创建对象
    const xhr = new XMLHttpRequest();

    // 初始化 设置类型和url
    xhr.open('POST', 'http://127.0.0.1:3000/register');

    // 设置请求头并发送数据
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        username: register_phone_value,
        password: register_password_value,
        password_confirm: register_password_confrim_value
    }));

    // 事件绑定
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                alert('注册成功!');
            }
            else if (xhr.status == 400){
                alert('两次密码输入不相同，请重新输入！');
            } 
            else if (xhr.status == 450) {
                alert('该用户已注册！');
            } 
            else {
                alert('注册失败，请重试！');
            }
        }
    }
}


// 登录
upload_btn.onclick = function (e) {
    var upload_phone_value = upload_phone.value;
    var upload_password_value = upload_password.value;

    // 创建对象
    const xhr = new XMLHttpRequest();

    // 初始化 设置类型和url
    xhr.open('POST', 'http://127.0.0.1:3000/upload');

    // 设置请求头并发送数据
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        username: upload_phone_value,
        password: upload_password_value
    }));

    // 事件绑定
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status >= 200 && xhr.status < 300) {
                alert('登录成功!');
            }
            else {
                alert('未找到该用户信息，请重试！');
            }
        }
    }
}
