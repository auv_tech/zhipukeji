const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
var moment = require('moment');

const app = express();
app.use(express.static('public'));
const port = 3000;
// 解析请求体的JSON数据
app.use(bodyParser.json());
app.use(express.json());

// 创建数据库连接
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin',
    password: '123qwe',
    database: 'webdata',
    port: 3306
});

// 连接到数据库
connection.connect((err) => {
    if (err) {
        console.log(err);
        console.log('Connect error!');
        return;
    }
    console.log('Connected to MySQL database');
});

// 处理浏览器端的POST请求
//注册
app.post('/register', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let password_confirm=req.body.password_confirm;
    var register_time = moment().format('YYYY-MM-DD HH:mm:ss');
    if(password==password_confirm){
        connection.query(`SELECT * FROM users WHERE username = ? AND password = ?`, [username, password], (err, rows) => {
            if (err) {
                console.log('Query error:', err); // 添加错误日志
                res.status(500).send({ error: 'Query error' });
                return;
            }
            if (rows.length > 0) {
                console.log('This user is already registered.');
                res.sendStatus(450);
            } 
            else{
                connection.query(`INSERT INTO users (username, password, register_time) VALUES (?, ?, ?)`,
                    [username, password, register_time], (err, result) => {
                        if (err) {
                            console.log('Query error:', err); // 添加错误日志
                            res.status(500).send({ error: 'Query error' });
                            return;
                    }
                    console.log('Insert success');
                    res.sendStatus(200);
                });
            }
    })}
    else{
        res.status(400).send({ error: 'Query error' });
        return;
    }
});

    

//登录

app.post('/upload', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;

    connection.query(`SELECT * FROM users WHERE username = ? AND password = ?`, [username, password], (err, rows) => {
        if (err) {
            console.log('Query error:', err); // 添加错误日志
            res.status(500).send({ error: 'Query error' });
            return;
        }

        if (rows.length > 0) {
            console.log('Login success');
            res.sendStatus(200);
        } else {
            console.log('Login failed');
            res.status(400).send({ error: 'Login failed' });
        }
    });
});


app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
