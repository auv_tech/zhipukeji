// 服务
var service_mainpage = document.getElementById("service_mainpage");
range[6].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    service_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var service_nav = document.getElementsByClassName("service_nav");
var service_directlink = document.getElementById("service_directlink");
service_nav[0].onclick = function (e) {
    service_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[0].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 0) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
service_nav[1].onclick = function (e) {
    service_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[1].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 1) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
service_nav[2].onclick = function (e) {
    service_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[2].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 2) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
service_nav[3].onclick = function (e) {
    service_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[3].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 3) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
service_nav[4].onclick = function (e) {
    service_nav[4].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[4].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 4) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
service_nav[5].onclick = function (e) {
    service_nav[5].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[5].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 5) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
service_nav[6].onclick = function (e) {
    service_nav[6].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    service_nav[6].style.color = 'black';
    for (var j = 0; j < service_nav.length; j++) {
        if (j != 6) {
            service_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            service_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
var service_more = document.getElementById("service_more");
var service_more_font = document.getElementById("service_more_font");
var service_directlink_products = document.getElementsByClassName("range_total_directlink_products_service");

service_more.onclick = function (e) {
    if (service_more_font.textContent == '更多') {
        service_directlink.style.height = '160px';
        service_directlink.style.transition = '0.5s';
        service_more_font.style.marginTop = '130px';
        service_more_font.style.transition = '0.5s';
        service_more.style.marginTop = '125px';
        service_more.style.transition = '0.5s';
        service_more_font.textContent = '收起';
        service_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= service_directlink_products.length; k++) {
            service_directlink_products[k].style.display = 'flex';
            service_directlink_products[k].style.transition = '0.5s';
            service_directlink_products[k].style.opacity = '1';
        }
    }
    else if (service_more_font.textContent == '收起') {
        service_directlink.style.height = '80px';
        service_directlink.style.transition = '0.5s';
        service_more_font.style.marginTop = '50px';
        service_more_font.style.transition = '0.5s';
        service_more.style.marginTop = '45px';
        service_more.style.transition = '0.5s';
        service_more_font.textContent = '更多';
        service_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= service_directlink_products.length; k++) {
            service_directlink_products[k].style.display = 'flex';
            service_directlink_products[k].style.transition = '0.5s';
            service_directlink_products[k].style.opacity = '0';
        }
    }
}
var service_choices = document.getElementsByClassName("service_products_choice");
service_choices[0].onclick = function (e) {
    service_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    service_choices[0].style.color = 'black';
    service_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    service_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    service_choices[1].style.color = 'rgba(255,255,255,0.8)';
    service_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    service_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    service_choices[2].style.color = 'rgba(255,255,255,0.8)';
    service_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
service_choices[1].onclick = function (e) {
    service_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    service_choices[1].style.color = 'black';
    service_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    service_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    service_choices[0].style.color = 'rgba(255,255,255,0.8)';
    service_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    service_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    service_choices[2].style.color = 'rgba(255,255,255,0.8)';
    service_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var service_choices2 = document.getElementById('service_choice3');
    if (service_choices2) {
        service_choices2.addEventListener('click', function () {
            service_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            service_choices2.style.color = 'black';
            service_choices2.style.border = '1px solid rgba(255,255,255,0)';
            service_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            service_choices[0].style.color = 'rgba(255,255,255,0.8)';
            service_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            service_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            service_choices[1].style.color = 'rgba(255,255,255,0.8)';
            service_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
service_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(service_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        service_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        service_choices[3].style.color = 'black';
        service_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        service_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        service_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        service_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}
