// 教育
var education_mainpage = document.getElementById("education_mainpage");
range[3].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    education_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var education_nav = document.getElementsByClassName("education_nav");
var education_directlink = document.getElementById("education_directlink");
var education_directlink_products = document.getElementsByClassName("range_total_directlink_products_education");

education_nav[0].onclick = function (e) {
    education_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    education_nav[0].style.color = 'black';
    for (var j = 0; j < education_nav.length; j++) {
        if (j != 0) {
            education_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            education_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
education_nav[1].onclick = function (e) {
    education_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    education_nav[1].style.color = 'black';
    for (var j = 0; j < education_nav.length; j++) {
        if (j != 1) {
            education_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            education_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
education_nav[2].onclick = function (e) {
    education_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    education_nav[2].style.color = 'black';
    for (var j = 0; j < education_nav.length; j++) {
        if (j != 2) {
            education_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            education_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
education_nav[3].onclick = function (e) {
    education_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    education_nav[3].style.color = 'black';
    for (var j = 0; j < education_nav.length; j++) {
        if (j != 3) {
            education_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            education_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
education_nav[4].onclick = function (e) {
    education_nav[4].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    education_nav[4].style.color = 'black';
    for (var j = 0; j < education_nav.length; j++) {
        if (j != 4) {
            education_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            education_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
education_nav[5].onclick = function (e) {
    education_nav[5].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    education_nav[5].style.color = 'black';
    for (var j = 0; j < education_nav.length; j++) {
        if (j != 5) {
            education_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            education_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
var education_more = document.getElementById("education_more");
var education_more_font = document.getElementById("education_more_font");
education_more.onclick = function (e) {
    if (education_more_font.textContent == '更多') {
        education_directlink.style.height = '240px';
        education_directlink.style.transition = '0.5s';
        education_more_font.style.marginTop = '210px';
        education_more_font.style.transition = '0.5s';
        education_more.style.marginTop = '205px';
        education_more.style.transition = '0.5s';
        education_more_font.textContent = '收起';
        education_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= education_directlink_products.length; k++) {
            education_directlink_products[k].style.display = 'flex';
            education_directlink_products[k].style.transition = '0.5s';
            education_directlink_products[k].style.opacity = '1';
        }
    }
    else if (education_more_font.textContent == '收起') {
        education_directlink.style.height = '80px';
        education_directlink.style.transition = '0.5s';
        education_more_font.style.marginTop = '50px';
        education_more_font.style.transition = '0.5s';
        education_more.style.marginTop = '45px';
        education_more.style.transition = '0.5s';
        education_more_font.textContent = '更多';
        education_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= education_directlink_products.length; k++) {
            education_directlink_products[k].style.display = 'flex';
            education_directlink_products[k].style.transition = '0.5s';
            education_directlink_products[k].style.opacity = '0';
        }
    }
}
var education_choices = document.getElementsByClassName("education_products_choice");
education_choices[0].onclick = function (e) {
    education_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    education_choices[0].style.color = 'black';
    education_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    education_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    education_choices[1].style.color = 'rgba(255,255,255,0.8)';
    education_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    education_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    education_choices[2].style.color = 'rgba(255,255,255,0.8)';
    education_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
education_choices[1].onclick = function (e) {
    education_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    education_choices[1].style.color = 'black';
    education_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    education_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    education_choices[0].style.color = 'rgba(255,255,255,0.8)';
    education_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    education_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    education_choices[2].style.color = 'rgba(255,255,255,0.8)';
    education_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var education_choices2 = document.getElementById('education_choice3');
    if (education_choices2) {
        education_choices2.addEventListener('click', function () {
            education_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            education_choices2.style.color = 'black';
            education_choices2.style.border = '1px solid rgba(255,255,255,0)';
            education_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            education_choices[0].style.color = 'rgba(255,255,255,0.8)';
            education_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            education_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            education_choices[1].style.color = 'rgba(255,255,255,0.8)';
            education_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
education_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(education_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        education_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        education_choices[3].style.color = 'black';
        education_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        education_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        education_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        education_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}
