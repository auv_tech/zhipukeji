// 电信
var dianxin_mainpage = document.getElementById("dianxin_mainpage");
range[1].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    dianxin_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var dianxin_nav = document.getElementsByClassName("dianxin_nav");
var dianxin_directlink = document.getElementById("dianxin_directlink");
var dianxin_back = document.getElementById("dianxin_back");

dianxin_back.onclick = function () {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'flex';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'flex';
    fanhui.style.display = 'none';
    dianxin_mainpage.style.display = 'none';
    mainpage.style.display = 'flex';
    setting_wrap.style.display = 'flex';
}

dianxin_nav[0].onclick = function (e) {
    dianxin_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    dianxin_nav[0].style.color = 'black';
    for (var j = 0; j < dianxin_nav.length; j++) {
        if (j != 0) {
            dianxin_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            dianxin_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
dianxin_nav[1].onclick = function (e) {
    dianxin_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    dianxin_nav[1].style.color = 'black';
    for (var j = 0; j < dianxin_nav.length; j++) {
        if (j != 1) {
            dianxin_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            dianxin_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
dianxin_nav[2].onclick = function (e) {
    dianxin_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    dianxin_nav[2].style.color = 'black';
    for (var j = 0; j < dianxin_nav.length; j++) {
        if (j != 2) {
            dianxin_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            dianxin_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
dianxin_nav[3].onclick = function (e) {
    dianxin_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    dianxin_nav[3].style.color = 'black';
    for (var j = 0; j < dianxin_nav.length; j++) {
        if (j != 3) {
            dianxin_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            dianxin_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
var dianxin_more = document.getElementById("dianxin_more");
var dianxin_more_font = document.getElementById("dianxin_more_font");
var dianxin_directlink_products = document.getElementsByClassName("range_total_directlink_products_dianxin");
console.log(dianxin_directlink_products);
dianxin_more.onclick = function (e) {
    if (dianxin_more_font.textContent == '更多') {
        dianxin_directlink.style.height = '160px';
        dianxin_directlink.style.transition = '0.5s';
        dianxin_more_font.style.marginTop = '130px';
        dianxin_more_font.style.transition = '0.5s';
        dianxin_more.style.marginTop = '125px';
        dianxin_more.style.transition = '0.5s';
        dianxin_more_font.textContent = '收起';
        dianxin_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= dianxin_directlink_products.length; k++) {
            dianxin_directlink_products[k].style.display = 'flex';
            dianxin_directlink_products[k].style.transition = '0.5s';
            dianxin_directlink_products[k].style.opacity = '1';
        }
    }
    else if (dianxin_more_font.textContent == '收起') {
        dianxin_directlink.style.height = '80px';
        dianxin_directlink.style.transition = '0.5s';
        dianxin_more_font.style.marginTop = '50px';
        dianxin_more_font.style.transition = '0.5s';
        dianxin_more.style.marginTop = '45px';
        dianxin_more.style.transition = '0.5s';
        dianxin_more_font.textContent = '更多';
        dianxin_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= dianxin_directlink_products.length; k++) {
            dianxin_directlink_products[k].style.display = 'flex';
            dianxin_directlink_products[k].style.transition = '0.5s';
            dianxin_directlink_products[k].style.opacity = '0';
        }
    }
}
var dianxin_choices = document.getElementsByClassName("dianxin_products_choice");
dianxin_choices[0].onclick = function (e) {
    dianxin_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    dianxin_choices[0].style.color = 'black';
    dianxin_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    dianxin_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    dianxin_choices[1].style.color = 'rgba(255,255,255,0.8)';
    dianxin_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    dianxin_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    dianxin_choices[2].style.color = 'rgba(255,255,255,0.8)';
    dianxin_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
dianxin_choices[1].onclick = function (e) {
    dianxin_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    dianxin_choices[1].style.color = 'black';
    dianxin_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    dianxin_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    dianxin_choices[0].style.color = 'rgba(255,255,255,0.8)';
    dianxin_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    dianxin_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    dianxin_choices[2].style.color = 'rgba(255,255,255,0.8)';
    dianxin_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var dianxin_choices2 = document.getElementById('dianxin_choice3');
    if (dianxin_choices2) {
        dianxin_choices2.addEventListener('click', function () {
            dianxin_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            dianxin_choices2.style.color = 'black';
            dianxin_choices2.style.border = '1px solid rgba(255,255,255,0)';
            dianxin_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            dianxin_choices[0].style.color = 'rgba(255,255,255,0.8)';
            dianxin_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            dianxin_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            dianxin_choices[1].style.color = 'rgba(255,255,255,0.8)';
            dianxin_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
dianxin_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(dianxin_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        dianxin_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        dianxin_choices[3].style.color = 'black';
        dianxin_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        dianxin_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        dianxin_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        dianxin_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}