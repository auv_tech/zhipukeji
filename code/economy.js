// 金融
var economy_mainpage = document.getElementById("economy_mainpage");
range[2].onclick = function (e) {
    ask.style.display = 'none';
    choiceleft.style.display = 'none';
    choiceright.style.display = 'none';
    explainleft.style.display = 'none';
    explainright.style.display = 'none';
    range_wrap.style.display = 'none';
    fanhui.style.display = 'none';
    economy_mainpage.style.display = 'block';
    mainpage.style.display = 'none';
    setting_wrap.style.display = 'none'
}
var economy_nav = document.getElementsByClassName("economy_nav");
var economy_directlink = document.getElementById("economy_directlink");
economy_nav[0].onclick = function (e) {
    economy_nav[0].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    economy_nav[0].style.color = 'black';
    for (var j = 0; j < economy_nav.length; j++) {
        if (j != 0) {
            economy_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            economy_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
economy_nav[1].onclick = function (e) {
    economy_nav[1].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    economy_nav[1].style.color = 'black';
    for (var j = 0; j < economy_nav.length; j++) {
        if (j != 1) {
            economy_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            economy_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
economy_nav[2].onclick = function (e) {
    economy_nav[2].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    economy_nav[2].style.color = 'black';
    for (var j = 0; j < economy_nav.length; j++) {
        if (j != 2) {
            economy_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            economy_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
economy_nav[3].onclick = function (e) {
    economy_nav[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    economy_nav[3].style.color = 'black';
    for (var j = 0; j < economy_nav.length; j++) {
        if (j != 3) {
            economy_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            economy_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
economy_nav[4].onclick = function (e) {
    economy_nav[4].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
    economy_nav[4].style.color = 'black';
    for (var j = 0; j < economy_nav.length; j++) {
        if (j != 4) {
            economy_nav[j].style.backgroundColor = 'rgba(255, 255, 255, 0)';
            economy_nav[j].style.color = 'rgba(255, 255, 255, 0.8)';
        }
    }
}
var economy_more = document.getElementById("economy_more");
var economy_more_font = document.getElementById("economy_more_font");
var economy_directlink_products = document.getElementsByClassName("range_total_directlink_products_economy");

economy_more.onclick = function (e) {
    if (economy_more_font.textContent == '更多') {
        economy_directlink.style.height = '160px';
        economy_directlink.style.transition = '0.5s';
        economy_more_font.style.marginTop = '130px';
        economy_more_font.style.transition = '0.5s';
        economy_more.style.marginTop = '125px';
        economy_more.style.transition = '0.5s';
        economy_more_font.textContent = '收起';
        economy_more.style.transform = 'rotate(0deg)';
        for (var k = 8; k <= economy_directlink_products.length; k++) {
            economy_directlink_products[k].style.display = 'flex';
            economy_directlink_products[k].style.transition = '0.5s';
            economy_directlink_products[k].style.opacity = '1';
        }
    }
    else if (economy_more_font.textContent == '收起') {
        economy_directlink.style.height = '80px';
        economy_directlink.style.transition = '0.5s';
        economy_more_font.style.marginTop = '50px';
        economy_more_font.style.transition = '0.5s';
        economy_more.style.marginTop = '45px';
        economy_more.style.transition = '0.5s';
        economy_more_font.textContent = '更多';
        economy_more.style.transform = 'rotate(180deg)';
        for (var k = 8; k <= dianxin_directlink_products.length; k++) {
            economy_directlink_products[k].style.display = 'flex';
            economy_directlink_products[k].style.transition = '0.5s';
            economy_directlink_products[k].style.opacity = '0';
        }
    }
}
var economy_choices = document.getElementsByClassName("economy_products_choice");
economy_choices[0].onclick = function (e) {
    economy_choices[0].style.backgroundColor = 'rgba(255,255,255,0.8)';
    economy_choices[0].style.color = 'black';
    economy_choices[0].style.border = '1px solid rgba(255,255,255,0)';
    economy_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
    economy_choices[1].style.color = 'rgba(255,255,255,0.8)';
    economy_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
    economy_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    economy_choices[2].style.color = 'rgba(255,255,255,0.8)';
    economy_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
economy_choices[1].onclick = function (e) {
    economy_choices[1].style.backgroundColor = 'rgba(255,255,255,0.8)';
    economy_choices[1].style.color = 'black';
    economy_choices[1].style.border = '1px solid rgba(255,255,255,0)';
    economy_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
    economy_choices[0].style.color = 'rgba(255,255,255,0.8)';
    economy_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
    economy_choices[2].style.backgroundColor = 'rgba(255,255,255,0)';
    economy_choices[2].style.color = 'rgba(255,255,255,0.8)';
    economy_choices[2].style.border = '1px solid rgba(255,255,255,0.8)';
}
document.addEventListener('DOMContentLoaded', function () {
    var economy_choices2 = document.getElementById('economy_choice3');
    if (economy_choices2) {
        economy_choices2.addEventListener('click', function () {
            economy_choices2.style.backgroundColor = 'rgba(255,255,255,0.8)';
            economy_choices2.style.color = 'black';
            economy_choices2.style.border = '1px solid rgba(255,255,255,0)';
            economy_choices[0].style.backgroundColor = 'rgba(255,255,255,0)';
            economy_choices[0].style.color = 'rgba(255,255,255,0.8)';
            economy_choices[0].style.border = '1px solid rgba(255,255,255,0.8)';
            economy_choices[1].style.backgroundColor = 'rgba(255,255,255,0)';
            economy_choices[1].style.color = 'rgba(255,255,255,0.8)';
            economy_choices[1].style.border = '1px solid rgba(255,255,255,0.8)';
        });
    }
});
economy_choices[3].onclick = function (e) {
    var computedStyle = getComputedStyle(economy_choices[3]);
    var backgroundColor = computedStyle.backgroundColor;
    if (backgroundColor === 'rgba(0, 0, 0, 0)') {
        economy_choices[3].style.backgroundColor = 'rgba(255, 255, 255, 0.8)';
        economy_choices[3].style.color = 'black';
        economy_choices[3].style.border = '1px solid rgba(255, 255, 255, 0)';
    }
    else {
        economy_choices[3].style.backgroundColor = 'rgba(0, 0, 0, 0)';
        economy_choices[3].style.color = 'rgba(255, 255, 255, 0.9)';
        economy_choices[3].style.border = '1px solid rgba(255, 255, 255, 0.8)';
    }
}
